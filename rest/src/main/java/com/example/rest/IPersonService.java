package com.example.rest;

import java.util.List;

public interface IPersonService {

	String savePerson(Person person);
	List<Person> getPersons();
	String updatePerson(Person person);
	String patchPerson(Person person);
	String DeletePerson(Integer id);
	Person getPerson(Integer id);
}
