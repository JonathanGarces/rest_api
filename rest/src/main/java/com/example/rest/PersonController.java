package com.example.rest;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/api")
public class PersonController {
	
	
	@Autowired
	private IPersonService personService;
	
	@GetMapping("/string")
	public String getString(){
		
		return "Hi from restController";
	}
	
	
	@PostMapping("/person")
    public String savePerson(@RequestBody Person person){
		
		return personService.savePerson(person);
	}
	
	@GetMapping("/person")
    public List<Person> getPerson(){
		return personService.getPersons();
	}
	
	@PutMapping("/person")
    public Person updatePerson(@RequestBody Person person){
		
		personService.updatePerson(person);
		return personService.getPerson(person.getId());
	}
	
	@PatchMapping("/person")
    public Person patchPerson(@RequestBody Person person){
		
		personService.updatePerson(person);
		return personService.getPerson(person.getId());
	}
	
	@DeleteMapping("/person/{id}")
    public String deletePerson(@PathVariable(name = "id") Integer personId){
		
		return personService.DeletePerson(personId);
	}
	
	
	

}
