package com.example.rest;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

public interface PersonRepository extends JpaRepository<Person, Integer> {

	List<Person> findByAgeGreaterThanEqual(int age);
}
