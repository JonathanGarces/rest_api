package com.example.rest;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class PersonService implements IPersonService {

	@Autowired
	private PersonRepository personRepository;
	
	@Override
	public String savePerson(Person person) {
		// TODO Auto-generated method stub
		
		
		personRepository.save(person);
		return "Person saved successfully";
	}

	@Override
	public List<Person> getPersons() {
		// TODO Auto-generated method stub
		return personRepository.findByAgeGreaterThanEqual(18);
	}

	@Override
	public String updatePerson(Person person) {
		// TODO Auto-generated method stub
		personRepository.save(person);
		return null;
	}

	@Override
	public String patchPerson(Person person) {
		// TODO Auto-generated method stub
		personRepository.save(person);
		return null;
	}

	@Override
	public String DeletePerson(Integer id) {
		// TODO Auto-generated method stub
		
		personRepository.deleteById(id);
		Optional<Person> person = personRepository.findById(id);
		
		return person.isPresent() ? "Erro, person is not deleted" : "person deleted successfully";
	}

	@Override
	public Person getPerson(Integer id) {
		// TODO Auto-generated method stub
		Optional<Person> person = personRepository.findById(id);
		
		return person.isPresent() ? person.get() : null;
	}

}
